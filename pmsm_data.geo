// Permanent magnet synchronous machine
// Example of Prof. Dr. Mauricio Valencia Ferreira da Luz (Florianopolis, August 23, 2010)

// Modified and customised for Onelab by Ruth V. Sabariego (February, 2013)

mm = 1e-3 ;
deg2rad = Pi/180 ;

pp = "Input/Constructive parameters/";

A0 =  45 * deg2rad ; // stator angle length
A0R = A0; // rotor angle length

SlotAngle = A0 / 3;

DefineConstant[
  ResId = "",
  NbrPolesInModel = { 1, Choices {1="1", 2="2", 4="4", 8="8"},
    Name "Input/20Number of poles in FE model",
    Highlight "Blue", Visible 1},
  InitialRotorAngle_deg = {7.5,
    Name StrCat[ResId, "Input/21Start rotor angle [deg]"],
    Highlight "AliceBlue"}
] ;

//--------------------------------------------------------------------------------

InitialRotorAngle = InitialRotorAngle_deg*deg2rad ; // initial rotor angle, 0 if aligned

//------------------------------------------------
//------------------------------------------------
NbrPolesTot = 8 ; // number of poles in complete cross-section
NbrPolePairs = NbrPolesTot/2 ;

SymmetryFactor = NbrPolesTot/NbrPolesInModel ;
Flag_Symmetry = (SymmetryFactor==1)?0:1 ;

NbrSectTot = NbrPolesTot ; // number of "rotor teeth"
NbrSect = NbrSectTot*NbrPolesInModel/NbrPolesTot ; // number of "rotor teeth" in FE model
//--------------------------------------------------------------------------------

//------------------------------------------------
// Stator
//------------------------------------------------
NbrSectTotStator  = 24; // number of stator teeth
NbrSectStator   = NbrSectTotStator*NbrPolesInModel/NbrPolesTot; // number of stator teeth in FE model
//--------------------------------------------------------------------------------

// Magnet Thickness
lm0 = 2*mm;

// stator slot tip
w_SlotTip = 1 * mm;

// stator slot tip step
w_SlotStep = 1 * mm;

// slot face length
w_SlotFace = 4 * mm;

// slot bottom curve radius
w_SlotCurve = 1 * mm;

// slot relative apperture
k_Slot = 0.3;

// slot apperture angle
SlotApAngle = A0 / 3 * k_Slot;

// Tooth surface angle
ToothSurfAngle = A0 / 3 - SlotApAngle;

// Stator back width
w_StatorBack = 4 * mm;

// Tooth tickness
t_Tooth = 6 * mm;

// airgap
airgap = 0.8 * mm;

Th_magnet = A0R * 0.833;  // angle in degrees 0 < Th_magnet < 45

// Air gap rotor radius
rRairgap = 44 * mm;
// rotor shaft radius
rRshaft = 40 * mm;

axial_length = 35 * mm;

//
// Calculus
//

DefineConstant[
               lm = {lm0 , Name StrCat[pp, "Magnet height [m]"], Closed 0}
               ];

DefineConstant[
               AxialLength = {axial_length,  Name StrCat[pp, "Axial length [m]"], Closed 1},
               Gap = {airgap, Name StrCat[pp, "Airgap width [m]"], Closed 0}
               ];

If (rRairgap > rRshaft)
  sign = 1;
Else
  sign = -1;
EndIf

rR2 = (rRairgap - sign * lm); //23.243e-03;
rR3 = (rRairgap - sign * 0.7389*lm); //23.862e-03;
rR4 = (rRairgap - sign * 0.72278*lm); //23.9e-03;

// stator airgap radius
rSairgap = rRairgap + sign * Gap;
rS2 = rSairgap + sign * w_SlotTip;
rS3 = rS2 + sign * w_SlotStep;
rS4 = rS3 + sign * w_SlotFace; //rS4 = 38.16*mm;
rS5 = rS4; // It is useless!! it is wrong!!
rS6 = rS5 + sign * w_SlotCurve;
rS7 = rS6 + sign * w_StatorBack;

rB1  = rRairgap + sign * Gap / 3;
rB1b = rB1;
rB2  = rRairgap + sign * Gap * 2 / 3;


A1 =   0 * deg2rad ; // rotor angle offset

sigma_fe = 0. ; // laminated steel
DefineConstant[
  mur_fe = {1000, Name StrCat[pp, "Relative permeability for linear case"]},
  b_remanent = {1.2, Name StrCat[pp, "Remanent induction [T]"] }
];

rpm_nominal = 500 ;
Inominal = 10.9 ; // Nominal current
Tnominal = 2.5 ; // Nominal torque

// ----------------------------------------------------
// Numbers for physical regions in .geo and .pro files
// ----------------------------------------------------
// Rotor
ROTOR_FE     = 1000 ;
ROTOR_AIR    = 1001 ;
ROTOR_AIRGAP = 1002 ;
ROTOR_MAGNET = 1010 ; // Index for first Magnet (1/8 model->1; full model->8)

ROTOR_BND_MOVING_BAND = 1100 ; // Index for first line (1/8 model->1; full model->8)
ROTOR_BND_A0 = 1200 ;
ROTOR_BND_A1 = 1201 ;
SURF_INT     = 1202 ;

// Stator
STATOR_FE     = 2000 ;
STATOR_AIR    = 2001 ;
STATOR_AIRGAP = 2002 ;

STATOR_BND_MOVING_BAND = 2100 ;// Index for first line (1/8 model->1; full model->8)
STATOR_BND_A0          = 2200 ;
STATOR_BND_A1          = 2201 ;

STATOR_IND = 2300 ; //Index for first Ind (1/8 model->3; full model->24)
STATOR_IND_AP = STATOR_IND + 1 ; STATOR_IND_BM = STATOR_IND + 2 ;STATOR_IND_CP = STATOR_IND + 3 ;
STATOR_IND_AM = STATOR_IND + 4 ; STATOR_IND_BP = STATOR_IND + 5 ;STATOR_IND_CM = STATOR_IND + 6 ;

SURF_EXT = 3000 ; // outer boundary


MOVING_BAND = 9999 ;

NICEPOS = 111111 ;
